/**
 * Module dependencies.
 */
var express = require('express')
,	path = require('path')
,	streams = require('./app/streams.js')();

var favicon = require('serve-favicon')
,	logger = require('morgan')
,	methodOverride = require('method-override')
,	bodyParser = require('body-parser')
,	errorHandler = require('errorhandler');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(favicon(__dirname + '/public/images/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride());
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(errorHandler());
}
// routing
//require('./app/routes.js')(app, streams);
var displayStreams = function(req, res) {
	var streamList = streams.getStreams();
	// JSON exploit to clone streamList.public
	var data = (JSON.parse(JSON.stringify(streamList))); 

	res.status(200).json(data);
};

app.get('/streams.json', displayStreams);
app.get('/', function(req, res) {
	res.render('index', 
		{ title: 'Project RTC', header: 'WebRTC live streaming',
		  username: 'Username',share: 'Share this link',
		  footer: 'pierre@chabardes.net',id: '0'
		});
});

app.get('/:id', function(req, res) {
	res.render('index', 
		{ title: 'Project RTC', header: 'WebRTC live streaming',
		  username: 'Username',share: 'Share this link',
		  footer: 'pierre@chabardes.net',id: '0'
		});
});

var server = app.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

var io = require('socket.io').listen(server);
/**
 * Socket.io event handling
 */
require('./app/socketHandler.js')(io, streams);