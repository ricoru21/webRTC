## WebRTC Live Streaming

- Node.js server
- Desktop client

The signaling part is done with [socket.io](socket.io).
The client is built with [angularjs](https://angularjs.org/).

## Install

It requires [node.js](http://nodejs.org/download/)

The server will run on port 3000.
You can test it in the (Chrome or Firefox) browser at localhost:3000.

## Author
- [Pierre Chabardes](mailto:pierre@chabardes.net)

## Modificaciones
- [Richard Oruna](mailto:ricoru21@gmail.com)