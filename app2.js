var express = require('express')
,	path = require('path')
,	streams = require('./app/streams.js')();

var favicon = require('serve-favicon')
,	logger = require('morgan')
,	methodOverride = require('method-override')
,	bodyParser = require('body-parser')
,	errorHandler = require('errorhandler');

var app = express();

app.engine('.html', require('ejs').__express);
app.set('views', __dirname + '/views');
app.set('view engine', 'html');
app.set('port', process.env.PORT || 3000);
app.use(favicon(__dirname + '/public/images/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride());
app.use(express.static(path.join(__dirname, 'public')));

var server = app.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

var io = require('socket.io').listen(server);
/** Socket.io event handling **/
require('./app/socketHandler.js')(io, streams);

// development only
if ('development' == app.get('env')) {
  app.use(errorHandler());
}

var displayStreams = function(req, res) {
	var streamList = streams.getStreams();// JSON exploit to clone streamList.public
	var data = (JSON.parse(JSON.stringify(streamList))); 
	res.status(200).json(data);
};

app.get('/streams.json', displayStreams);
app.get('/', function(req, res) {
	res.render('index2',{ title: 'Project RTC', header: 'WebRTC live streaming',
		  username: 'Richard Oruna',share: 'compartir link',
		  footer: 'ricoru21@gmail.com',id: req.body.id
		});
});

app.get('/:id', function(req, res) {
	res.render('index', 
		{ title: 'Project RTC', header: 'WebRTC live streaming',
		  username: 'Richard Oruna',share: 'compartir link',
		  footer: 'ricoru21@gmail.com',id: req.body.id
		});
});

