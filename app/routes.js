module.exports = function(app, streams) {

  // GET home 
  /*app.set('views', __dirname + '/views');
  app.engine('html', require('ejs').renderFile);

  var index = function(req, res) {
    res.render('index.ejs', { title: 'Project RTC', header: 'WebRTC live streaming',
      username: 'Username',share: 'Share this link',footer: 'pierre@chabardes.net',id: 'req.params.id'});
  };*/
  /*app.get('/', function(req, res) {
    res.render(__dirname + '/index', {
      string: 'random_value',
      other: 'value'
    });
  });*/

  // GET streams as JSON
  var displayStreams = function(req, res) {
    var streamList = streams.getStreams();
    // JSON exploit to clone streamList.public
    var data = (JSON.parse(JSON.stringify(streamList))); 

    res.status(200).json(data);
  };

  app.get('/streams.json', displayStreams);
  app.get('/', function(req, res) {
    res.render('index', { title: 'Project RTC', header: 'WebRTC live streaming',username: 'Username',share: 'Share this link',footer: 'pierre@chabardes.net',id: 'req.params.id'});
    }
  );
  //app.get('/:id', index);
}